package ntnu.idatt2001.espenjus.wargamesPackage;

import ntnu.idatt2001.espenjus.units.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * class for file handling in Wargames
 */
public class WargamesFileHandler {

    private static final String DELIMITER = ",";
    private static final String NEWLINE = "\n";
    private final static String DLM = File.separator;
    private String filePath = "src" + DLM + "main" + DLM + "resources" + DLM + "ntnu" + DLM + "idatt2001" + DLM +
            "espenjus" + DLM + "View" + DLM + "wargamesFiles" + DLM;

    /**
     * empty constructor
     */
    public WargamesFileHandler() {}

    /**
     * method for saving an army to a file. Saves the file to filepath specified in class.
     * saves the file as the army's name + .csv
     * @param army The army you want to save to file
     * @throws IOException
     */
    public void saveArmyToFile(Army army) throws IOException {
        File file = new File(String.valueOf(getArmyFile(army.getName())));
        try (FileWriter writer = new FileWriter(file)) {
            writer.write(army.getName() + NEWLINE);
            for (Unit unit : army.getUnits()) {
                String line = unit.getClass().getSimpleName() + DELIMITER + unit.getName() + DELIMITER + unit.getHealth();
                writer.write(line + NEWLINE);
            }
        } catch (IOException e) {
            throw new IOException("Unable to write to file: " + e.getMessage());
        }
    }

    /**
     * method for reading an Army from file
     * @param armyName String name of the army you want to search for in files
     * @return The army read from file of type Army
     * @throws IOException
     */
    public Army readArmyFromFile(String armyName) throws IOException {
        // check if the file exists
        if (!getArmyFile(armyName).exists()) throw new IllegalArgumentException("No file found with name: " + armyName + ".csv");
        else {
            String nameOfArmy;
            List<Unit> unitList = new ArrayList<>();
            File file = getArmyFile(armyName);
            try (Scanner scanner = new Scanner(file)) {
                //sets name of Army to first line of file, throws exception if blank or empty
                nameOfArmy = scanner.nextLine();
                if (nameOfArmy.isEmpty() || nameOfArmy.isBlank()) throw new IllegalArgumentException("ArmyName on first line is missing in the file");
                // scans each line and adds a unit to unitList if there's no exceptions
                while (scanner.hasNext()) {
                    Unit nextUnit;
                    String line = scanner.nextLine();
                    String[] unitInfo = line.split(DELIMITER);
                    if (!(unitInfo.length == 3)) throw new IllegalArgumentException("Line data: " + line + " is invalid");
                    if (unitInfo[1].isBlank() || unitInfo[1].isEmpty()) throw new IllegalArgumentException("No name of unit at line: " + line);
                    try {
                        Integer.parseInt(unitInfo[2]);
                    } catch (NumberFormatException e) {
                        throw new IOException("Health value of line: " + line + " is missing, or not a number");
                    }
                        switch (unitInfo[0]) {
                            case "CavalryUnit": nextUnit = new CavalryUnit(unitInfo[1], Integer.parseInt(unitInfo[2]));
                                break;
                            case "InfantryUnit": nextUnit = new InfantryUnit(unitInfo[1], Integer.parseInt(unitInfo[2]));
                                break;
                            case "RangedUnit": nextUnit = new RangedUnit(unitInfo[1], Integer.parseInt(unitInfo[2]));
                                break;
                            case "CommanderUnit": nextUnit = new CommanderUnit(unitInfo[1], Integer.parseInt(unitInfo[2]));
                                break;
                            default: throw new IllegalArgumentException("The unit type " + unitInfo[0] + " does not exist from line: " + line);
                        }
                        unitList.add(nextUnit);
                }
            } catch (FileNotFoundException e) {
                throw new IOException("Unable to read army data from file: " + file.getName() + "': " + e.getMessage());
            }
            Army returnArmy = new Army(nameOfArmy, unitList);
            return returnArmy;
        }
    }


    /**
     * get method for the file path of the file handler
     * @return String path
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * set method for the file path if you want to store the file at a different location.
     * @param filePath String new filepath
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * method for getting the file of Army information which is linked to the army's name.
     * @param armyName String armyname
     * @return File of ArmyInfo
     */
    public File getArmyFile(String armyName) {
        return new File(getFilePath() + armyName.strip().trim() + ".csv");
    }

}
