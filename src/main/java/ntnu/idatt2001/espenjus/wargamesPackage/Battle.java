package ntnu.idatt2001.espenjus.wargamesPackage;

import ntnu.idatt2001.espenjus.units.Unit;

import java.util.*;

/**
 * class for a Battle between two armys
 */
public class Battle {

    private Army armyOne;
    private Army armyTwo;

    /**
     * constructor for the class
     * @param armyOne Army, one part of the battle
     * @param armyTwo Army, the other part of the battle.
     */
    public Battle(Army armyOne, Army armyTwo) {
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
    }

    /**
     * get method for first army
     * @return Army
     */
    public Army getArmyOne() {
        return armyOne;
    }

    /**
     * get method for second army
     * @return Army
     */
    public Army getArmyTwo() {
        return armyTwo;
    }

    /**
     * set-method for ArmyONe
     * @param armyOne Army
     */
    public void setArmyOne(Army armyOne) {
        this.armyOne = armyOne;
    }

    /**
     * set-method for ArmyTwo
     * @param armyTwo Army
     */
    public void setArmyTwo(Army armyTwo) {
        this.armyTwo = armyTwo;
    }



    /**
     * method for simulating a battle between the two armies. one random Unit from each army attacks one random Unit
     * from their rival until all the ntnu.idatt2001.espenjus.Units in one army are dead. The winning army is returned.
     * @return Army, the winning army of the simulation.
     */
    public Army simulate(Terrain terrain) {
        Random random = new Random();
        while (armyOne.hasUnits() && armyTwo.hasUnits()) {
            Unit randomArmyOne = armyOne.getRandom();
            Unit randomArmyTwo = armyTwo.getRandom();
            if (random.nextBoolean()) {
                randomArmyOne.attack(randomArmyTwo, terrain);
                if (randomArmyTwo.getHealth() == 0) {
                    armyTwo.remove(randomArmyTwo);
                }
            } else {
                randomArmyTwo.attack(randomArmyOne, terrain);
                if (randomArmyOne.getHealth() == 0) {
                    armyOne.remove(randomArmyOne);
                }
            }
        }
        if (armyOne.hasUnits()) {
            return armyOne;
        }
        if (armyTwo.hasUnits()) {
            return armyTwo;
        }
        return null;
    }

    /**
     * mathod for simulating until next kill in a battle simulation.
     * @param terrain
     * @return LinkedHashmap where the first key is the
     *      * army that attacked, and it's value is the attacking Unit. The second key is the army that was attacked
     *      * and the value is the Unit that was killed.
     */
    public LinkedHashMap<Army, Unit> SimulateNextKill(Terrain terrain) {
        LinkedHashMap<Army, Unit> unitsFromKill = new LinkedHashMap<>();
        Random random = new Random();
        while (unitsFromKill.isEmpty()) {
            Unit randomArmyOne = armyOne.getRandom();
            Unit randomArmyTwo = armyTwo.getRandom();
            if (random.nextBoolean()) {
                randomArmyOne.attack(randomArmyTwo, terrain);
                if (randomArmyTwo.getHealth() == 0) {
                    unitsFromKill.put(armyOne, randomArmyOne);
                    unitsFromKill.put(armyTwo, randomArmyTwo);
                    armyTwo.remove(randomArmyTwo);
                }
            } else {
                randomArmyTwo.attack(randomArmyOne, terrain);
                if (randomArmyOne.getHealth() == 0) {
                    unitsFromKill.put(armyTwo, randomArmyTwo);
                    unitsFromKill.put(armyOne, randomArmyOne);
                    armyOne.remove(randomArmyOne);
                }
            }
        }
        return unitsFromKill;
    }


    /**
     * to-String method for a Battle.
     * @return
     */
    public String toString() {
        return "Battle between " + this.armyOne.getName() + " and " + this.armyTwo.getName() + "!";
    }
}
