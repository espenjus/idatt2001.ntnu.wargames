package ntnu.idatt2001.espenjus.wargamesPackage;

import ntnu.idatt2001.espenjus.units.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

/**
 *class for an Army
 */
public class Army {

    private String name;
    private List<Unit> units = new ArrayList<>();

    /**
     * constructor for the class
     * @param name String representing the name of the army.
     * @param units List of ntnu.idatt2001.espenjus.Units in the army.
     */
    public Army(String name, List<Unit> units) {
        if (name.isBlank()) throw new IllegalArgumentException("name cannot be blank");
        this.name = name;
        this.units = units;
    }

    /**
     * constructor for creating an army with only name variable.
     * @param name String name
     */
    public Army(String name) {
        if (name.isBlank()) throw new IllegalArgumentException("name cannot be blank");
        this.name = name;
    }


    /**
     * get-method for the name variable
     * @return String name
     */
    public String getName() {
        return name;
    }

    /**
     * get-method for the list of ntnu.idatt2001.espenjus.Units
     * @return List<Unit> units
     */
    public List<Unit> getUnits() {
        return units;
    }

    /**
     * set method for Army name
     * @param name String name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * set method for Army Units
     * @param units List of Units
     */
    public void setUnits(List<Unit> units) {
        this.units = units;
    }

    /**
     * method for adding a unit to the army
     * @param unit the unit to be added
     */
    public void addUnit(Unit unit) {
        this.units.add(unit);
    }

    /**
     * method for adding a list of ntnu.idatt2001.espenjus.Units to an army
     * @param list List<Unit> list of ntnu.idatt2001.espenjus.Units to be added
     */
    public void addAll(ArrayList<Unit> list) {
        this.units.addAll(list);
    }

    /**
     * method for removing a unit from the army
     * @param unit unit to be removed
     * @return a boolean variable indicating if the Unit was removed or not.
     */
    public boolean remove(Unit unit) {
        if (units.contains(unit)) {
            units.remove(unit);
            return true;
        } else return false;
    }

    /**
     * method for checking if the army has ntnu.idatt2001.espenjus.Units
     * @return boolean
     */
    public boolean hasUnits() {
        if (this.units.size() > 0) {return true;}
        else return false;
    }

    /**
     * method for getting a random Unit from an army. returns null if there's no ntnu.idatt2001.espenjus.Units in the army
     * @return Unit, a random Unit
     */
    public Unit getRandom() {
        if (!this.units.isEmpty()) {
            Random random = new java.util.Random();
            int index = random.nextInt(this.units.size());
            return units.get(index);
        } else return null;
    }

    /**
     * Method for getting all InfantryUnits from an Army
     * @return List<Unit>
     */
    public List<Unit> getInfantryUnits() {
        return units.stream().filter(u -> u instanceof InfantryUnit).collect(Collectors.toList());
    }

    /**
     * method for getting all CavalryUnits from an Army
     * @return List<Unit>
     */
    public List<Unit> getCavalryUnits() {
        return units.stream().filter(u -> u.getClass().
                equals(CavalryUnit.class)).
                collect(Collectors.toList());
    }

    /**
     * method for getting all RangedUnits from an Army
     * @return List<Unit>
     */
    public List<Unit> getRangedUnits() {
        return units.stream().filter(u -> u instanceof RangedUnit).collect(Collectors.toList());
    }

    /**
     * method for getting all CommanderUnits from an Army
     * @return
     */
    public List<Unit> getCommanderUnits() {
        return units.stream().filter(u -> u instanceof CommanderUnit).collect(Collectors.toList());
    }


    /**
     * equals method
     * @param o Object to be compared to
     * @return boolean variable
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Army)) return false;
        Army army = (Army) o;
        return Objects.equals(this.name, army.name);
    }

    /**
     * to-String method for an army
     * @return String presenting an army
     */
    public String toString() {
        StringBuilder info = new StringBuilder();
        for (Unit u : units) {
            info.append(u.toString()).append("\n");
        }
        return "Name: " + name + "\n" + info;
    }

    /**
     * hashcode method by name
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(name);
    }



}
