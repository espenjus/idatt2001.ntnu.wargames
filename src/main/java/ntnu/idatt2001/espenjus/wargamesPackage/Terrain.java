package ntnu.idatt2001.espenjus.wargamesPackage;

/**
 * Enum Class for the different Terrains a battle could take place at
 */
public enum Terrain {
    HILL(3, "Hill", "RangedUnits will have an advantage when attacking in the Hills!"),
    PLAINS(2, "Plains", "CavalryUnits will have an advantage when attacking in the Plains!"),
    FOREST(2, "Forest",  "RangedUnits will have a disadvantage when attacking in the Forest. \n CavalryUnits will " +
            "have a disadvantage when attacked In the Forest. \n InfantryUnits will have an advantage both when attacking and when attacked in the Forest!");

    public final int bonus;
    public final String name;
    public final String description;


    /**
     * Constants for different Terrains
     * @param bonus int value that determines the terrains impact on the different Unit's getAttackBonus
     * and getResistBonus methods
     * @param name of the Terrain as String
     * @param description String description of the affect the different Terrains has on The units in Battle
     */
    Terrain(int bonus, String name, String description) {
        this.bonus = bonus;
        this.name = name;
        this.description = description;
    }
}
