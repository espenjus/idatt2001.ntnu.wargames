package ntnu.idatt2001.espenjus.controllers;


import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ntnu.idatt2001.espenjus.wargamesPackage.Army;
import ntnu.idatt2001.espenjus.wargamesPackage.Battle;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller for the MainPage
 */
public class MainPageController implements Initializable {


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    /**
     * method for loading ViewArmies page when corresponding button is pressed
     * @param event
     * @throws IOException if fxml load fails
     */
    public void changeToViewArmies(ActionEvent event) throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../view/ViewArmies.fxml"));
            Parent sceneParent = loader.load();
            Scene scene = new Scene(sceneParent);
            scene.setUserData(loader);
            ViewArmiesController controller = loader.getController();
            controller.initData(setUpBattle());

            Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.show();

        } catch (IOException e) {
            System.out.println(e.getMessage());
            throw e;
        }
    }

    /**
     * Sets up two empty Armies in a Battle that will be passed on to the ViewArmiesPage
     * @return a Battle
     */
   public Battle setUpBattle() {
        Army armyOne = new Army("ArmyOne");
        Army armyTwo = new Army("ArmyTwo");
        return new Battle(armyOne, armyTwo);
   }


}
