package ntnu.idatt2001.espenjus.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ntnu.idatt2001.espenjus.wargamesPackage.Army;
import ntnu.idatt2001.espenjus.wargamesPackage.Battle;
import ntnu.idatt2001.espenjus.wargamesPackage.WargamesFileHandler;

/**
 * Controller for the ViewArmies page
 */
public class ViewArmiesController implements Initializable {

    private Battle battle;
    private WargamesFileHandler fileHandler = new WargamesFileHandler();

    private String[] armies = {"Army one", "Army two"};

    @FXML
    private TextField ArmyFileName;

    @FXML
    private Label AddFromFileError, SaveToFileError, MissingUnitsError;

    @FXML
    private Label armyOneCavalry, armyOneInfantry, armyOneRanged, armyOneCommander;

    @FXML
    private Label armyTwoCavalry, armyTwoInfantry, armyTwoRanged, armyTwoCommander;

    @FXML
    private Label ArmyOneName, ArmyTwoName;

    @FXML
    private ChoiceBox<String> addFromFileBox, addFromFileBox1;



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //sets up the option on what army to load to when loading army from file
        addFromFileBox.getItems().addAll(armies);
        addFromFileBox.setValue(addFromFileBox.getItems().get(0));
        //sets up option on what army to save to file
        addFromFileBox1.getItems().addAll(armies);
        addFromFileBox1.setValue(addFromFileBox1.getItems().get(0));
    }

    /**
     * method that is called when changing to the ViewArmies page
     * @param battle the battle of the Wargames
     */
    public void initData(Battle battle) {
        this.battle = battle;
        setArmyOneData();
        setArmyTwoData();
    }

    /**
     * method for changing scene to EditArmyPage when corresponding button is pressed
     * @param actionEvent
     * @throws IOException if fxml load fails
     */
    public void goToEditArmy1Page(ActionEvent actionEvent) throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../view/EditArmy.fxml"));
            Parent sceneParent = loader.load();
            Scene scene = new Scene(sceneParent);
            scene.setUserData(loader);
            EditArmyController controller = loader.getController();
            //sends the info of the battle and the info of the army to edit to the controller
            controller.initData(battle, battle.getArmyOne());

            Stage window = (Stage) ((Node)actionEvent.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.show();

        } catch (IOException e) {
            System.out.println(e.getMessage());
            throw e;
        }
    }

    /**
     * method for changing scene to EditArmy page when corresponding button is pressed
     * @param actionEvent
     * @throws IOException
     */
    public void GoToEditArmy2Page(ActionEvent actionEvent) throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../view/EditArmy.fxml"));
            Parent sceneParent = loader.load();
            Scene scene = new Scene(sceneParent);
            scene.setUserData(loader);
            EditArmyController controller = loader.getController();
            //sends the info of the battle and the info of the army to edit to the controller
            controller.initData(battle, battle.getArmyTwo());
            //informs the EditArmy page that the army it is editing is not the first army of the battle
            controller.setEditedArmyIsArmyOne(false);

            Stage window = (Stage) ((Node)actionEvent.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.show();

        } catch (IOException e) {
            System.out.println(e.getMessage());
            throw e;
        }
    }

    /**
     * method for setting Information of the battle's first army to its Label fields
     */
    public void setArmyOneData() {

        Army armyOne = battle.getArmyOne();
        ArmyOneName.setText(armyOne.getName());

        armyOneCavalry.setText(String.valueOf(armyOne.getCavalryUnits().size()));
        armyOneInfantry.setText(String.valueOf(armyOne.getInfantryUnits().size()));
        armyOneRanged.setText(String.valueOf(armyOne.getRangedUnits().size()));
        armyOneCommander.setText(String.valueOf(armyOne.getCommanderUnits().size()));
    }

    /**
     * method for setting information of the battle's second army to its Label fiels.
     */
    public void setArmyTwoData() {

        Army armyTwo = battle.getArmyTwo();
        ArmyTwoName.setText(armyTwo.getName());

        armyTwoCavalry.setText(String.valueOf(armyTwo.getCavalryUnits().size()));
        armyTwoInfantry.setText(String.valueOf(armyTwo.getInfantryUnits().size()));
        armyTwoRanged.setText(String.valueOf(armyTwo.getRangedUnits().size()));
        armyTwoCommander.setText(String.valueOf(armyTwo.getCommanderUnits().size()));
    }

    /**
     * method for addind an Army from file when corresponding button is pressed
     * @param event
     */
    public void AddArmyFromFile(ActionEvent event) {
        AddFromFileError.setText("");
        SaveToFileError.setText("");
        //checks if the fields to search for an Army is filled and informs user if not
        if (ArmyFileName.getText().isEmpty() || ArmyFileName.getText().isBlank()){
            AddFromFileError.setText("Please input the name of the army!");
        } else {
            //tries to add Army from file to the army specified in the addFromFileBox ChoiceBox and informs user
            //if it succeeded.
            try {
                Army army = fileHandler.readArmyFromFile(ArmyFileName.getText());
                if (addFromFileBox.getValue().equals("Army one")) {
                    battle.setArmyOne(army);
                    setArmyOneData();
                    AddFromFileError.setText("Successfully added " + army.getName() + "!");
                } else {
                    battle.setArmyTwo(army);
                    setArmyTwoData();
                    AddFromFileError.setText("Successfully added " + army.getName() + "!");
                }
                //informs user if something went wrong
            } catch (IOException e) {
                e.printStackTrace();
                AddFromFileError.setText(e.getMessage());
            } catch (IllegalArgumentException i) {
                AddFromFileError.setText(i.getMessage());
            }
        }
    }

    /**
     * method for saving an Army to file when corresponding button is pressed
     * @param event
     * @throws IOException
     */
    public void SaveToFile(ActionEvent event) throws IOException {
        AddFromFileError.setText("");
        SaveToFileError.setText("");
        //checks if The army you want to save has units and informs user if not. Saves Army to file
        //with the Army's name as filename and informs user.
        try {
            if (addFromFileBox1.getValue().equals("Army one")) {
                if (!battle.getArmyOne().hasUnits()) {
                    SaveToFileError.setText(battle.getArmyOne().getName() + " has no units to save");
                } else {
                    fileHandler.saveArmyToFile(battle.getArmyOne());
                    SaveToFileError.setText("Successfully saved " + battle.getArmyOne().getName() + ".csv");
                }
            } else {
                if (!battle.getArmyTwo().hasUnits()) {
                    SaveToFileError.setText(battle.getArmyTwo().getName() + " has no units to save");
                } else {
                    fileHandler.saveArmyToFile(battle.getArmyTwo());
                    SaveToFileError.setText("Successfully saved " + battle.getArmyTwo().getName() + ".csv");
                }

            }
            //informs user if something went wrong
        } catch (IOException e) {
            SaveToFileError.setText(e.getMessage());
        }

    }

    /**
     * method for changing scene to the ChooseBattleInfo page when corresponding button is pressed
     * @param actionEvent
     * @throws IOException
     */
    public void GoToChooseBattleInfo(ActionEvent actionEvent) throws IOException {
        //checks if both Armies has Units to fight each other with and informs user if not
        if (battle.getArmyOne().getUnits().isEmpty()) {
            MissingUnitsError.setText(battle.getArmyOne().getName() + " need Units to fight!");
        } else if (battle.getArmyTwo().getUnits().isEmpty()) {
            MissingUnitsError.setText(battle.getArmyTwo().getName() + " need Units to fight!");
        } else {
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("../view/ChooseBattleInfo.fxml"));
                Parent sceneParent = loader.load();
                Scene scene = new Scene(sceneParent);
                scene.setUserData(loader);
                ChooseBattleInfoController controller = loader.getController();
                controller.initData(battle);

                Stage window = (Stage) ((Node)actionEvent.getSource()).getScene().getWindow();
                window.setScene(scene);
                window.show();

            } catch (IOException e) {
                System.out.println(e.getMessage());
                throw e;
            }
        }
    }
}
