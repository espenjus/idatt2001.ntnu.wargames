package ntnu.idatt2001.espenjus.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import ntnu.idatt2001.espenjus.units.Unit;
import ntnu.idatt2001.espenjus.wargamesPackage.Army;
import ntnu.idatt2001.espenjus.wargamesPackage.Battle;
import ntnu.idatt2001.espenjus.wargamesPackage.Terrain;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.ResourceBundle;

/**
 * Controller for Battle poge
 */
public class BattleController implements Initializable {

    private Battle battle;
    private Battle copyBattle;
    private Army armyOne = new Army("army1");
    private Army armyTwo = new Army("army2");
    private Terrain terrain;
    private static double numberOfUnitsArmyOne;
    private static double numberOfUnitsArmyTwo;

    @FXML
    private Button NextKillButton, FinishBattleButton;

    @FXML
    private AnchorPane Background;

    @FXML
    private Label KillOne, KillTwo, KillThree, KillFour, KillFive, KillSix;

    @FXML
    private Label ArmyOneName, ArmyTwoName, BattleTitle;

    @FXML
    private Label ArmyOneCav, ArmyOneInf, ArmyOneRan, ArmyOneCom;

    @FXML
    private Label ArmyTwoCav, ArmyTwoInf, ArmyTwoRan, ArmyTwoCom;

    @FXML
    private ProgressBar ArmyOneProgress, ArmyTwoProgress;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    /**
     * method for getting the needed information from past page
     * @param battle
     * @param terrain
     * @param terrainImage
     */
    public void initData(Battle battle, Terrain terrain, Image terrainImage) {
        this.battle = battle;
        //creates a copyBattle to make sure info of the battle is not lost
        armyOne.getUnits().addAll(battle.getArmyOne().getUnits());
        armyOne.setName(battle.getArmyOne().getName());
        armyTwo.getUnits().addAll(battle.getArmyTwo().getUnits());
        armyTwo.setName(battle.getArmyTwo().getName());
        copyBattle = new Battle(armyOne, armyTwo);
        //sets Terrain
        this.terrain = terrain;
        //sets info of how many Units each Army is starting the Battle with
        numberOfUnitsArmyOne = battle.getArmyOne().getUnits().size();
        numberOfUnitsArmyTwo = battle.getArmyTwo().getUnits().size();
        //sets the data of the Armies to their Labels and ProgressBar
        setArmyOneData(copyBattle.getArmyOne());
        setArmyTwoData(copyBattle.getArmyTwo());
        BattleTitle.setText("Battle of the " + terrain + "!");
        //sets Background to mach the Terrain of the Battle
        BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, false);
        BackgroundImage backgroundImage = new BackgroundImage(terrainImage, BackgroundRepeat.REPEAT,
                BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
        Background background = new Background(backgroundImage);
        Background.setBackground(background);
    }

    /**
     * Method that changes scene to ViewArmies page when corresponding button is pressed
     * @param event
     * @throws IOException
     */
    public void ReturnToViewArmies(ActionEvent event) throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../view/ViewArmies.fxml"));
            Parent sceneParent = loader.load();
            Scene scene = new Scene(sceneParent);
            scene.setUserData(loader);
            ViewArmiesController controller = loader.getController();
            controller.initData(battle);

            Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.show();

        } catch (IOException e) {
            System.out.println(e.getMessage());
            throw e;
        }
    }

    /**
     * Method that sets the Labels and ProgressBar of ArmyOne in Battle to the info from variable army
     * @param army Army
     */
    public void setArmyOneData(Army army) {
        ArmyOneName.setText(army.getName());
        ArmyOneCav.setText(String.valueOf(army.getCavalryUnits().size()));
        ArmyOneInf.setText(String.valueOf(army.getInfantryUnits().size()));
        ArmyOneRan.setText(String.valueOf(army.getRangedUnits().size()));
        ArmyOneCom.setText(String.valueOf(army.getCommanderUnits().size()));
        ArmyOneProgress.setProgress((copyBattle.getArmyOne().getUnits().size() / numberOfUnitsArmyOne));
    }

    /**
     * method that sets the Labels and Progressbar of ArmyTwo in Battle to the info from army
     * @param army Army
     */
    public void setArmyTwoData(Army army) {
        ArmyTwoName.setText(armyTwo.getName());
        ArmyTwoCav.setText(String.valueOf(army.getCavalryUnits().size()));
        ArmyTwoInf.setText(String.valueOf(army.getInfantryUnits().size()));
        ArmyTwoRan.setText(String.valueOf(army.getRangedUnits().size()));
        ArmyTwoCom.setText(String.valueOf(army.getCommanderUnits().size()));
        ArmyTwoProgress.setProgress(copyBattle.getArmyTwo().getUnits().size() / numberOfUnitsArmyTwo);
    }

    /**
     *method that simulates the rest of the Battle and displays the result when corresponding
     * button is pressed
     * @param actionEvent
     */
    public void FinishBattle(ActionEvent actionEvent) {
        //simulates the rest of Battle
        Army winner = copyBattle.simulate(terrain);
        BattleTitle.setText(winner.getName() + " won the battle with " + winner.getUnits().size() + " Unit(s) left!");
        //sets info of the Armies after Battle
        setArmyOneData(copyBattle.getArmyOne());
        setArmyTwoData(copyBattle.getArmyTwo());
        //makes Battle buttons unpressable
        NextKillButton.setDisable(true);
        FinishBattleButton.setDisable(true);
        //Fills battle feed with message
        String message = "Congratulations!";
        KillOne.setText(message); KillTwo.setText(message); KillThree.setText(message);
        KillFour.setText(message); KillFive.setText(message); KillSix.setText(message);
    }

    /**
     * method that simulates to next kill and updates Battle page with information when corresponding
     * button is pressed
     * @param actionEvent
     */
    public void SimulationOfNextKill(ActionEvent actionEvent){
        //gets attacking army, attacked army, attacking Unit and killed Unit
        LinkedHashMap<Army, Unit> attack = copyBattle.SimulateNextKill(terrain);
        Army attackingArmy = attack.keySet().iterator().next();
        Army attackedArmy;
        if (attackingArmy.equals(copyBattle.getArmyOne())) {
            attackedArmy = copyBattle.getArmyTwo();
        } else {
            attackedArmy = copyBattle.getArmyOne();
        }
        Unit attackingUnit = attack.get(attackingArmy);
        Unit killedUnit = attack.get(attackedArmy);
        // fills the battle feed with the newest kill of the Battle
        fillBattleFeed(attackingArmy, attackedArmy, attackingUnit, killedUnit);
        //updates Battle page with information about the armies
        setArmyOneData(copyBattle.getArmyOne());
        setArmyTwoData(copyBattle.getArmyTwo());
        //Checks if one of the Armies won the battle after the kill
        checkForWin(copyBattle);
    }

    /**
     * method for filling the battle feed with information about the latest kill
     * @param attackerArmy
     * @param attackedArmy
     * @param attackingUnit
     * @param killed
     */
    public void fillBattleFeed(Army attackerArmy, Army attackedArmy, Unit attackingUnit, Unit killed) {
        //Updates the top Label of the battle feed if it is the first kill of the Battle
        if (KillOne.getText().isEmpty()) {
            KillOne.setText(attackingUnit.getName() + " from " + attackerArmy.getName() + " killed " + killed.getName() + " from " + attackedArmy.getName() + "!");
            //updates the top Label of battle feed with the latest kill, and shoves previous kills one place further down
        } else {
            KillSix.setText(KillFive.getText());
            KillFive.setText(KillFour.getText());
            KillFour.setText(KillThree.getText());
            KillThree.setText(KillTwo.getText());
            KillTwo.setText(KillOne.getText());
            KillOne.setText(attackingUnit.getName() + " from " + attackerArmy.getName() + " killed " + killed.getName() + " from " + attackedArmy.getName() + "!");
        }
    }

    /**
     * Method that checks if an Army has won the battle
     * @param battle
     */
    public void checkForWin(Battle battle) {
        //if ArmyOne has no units, the Page is updated with information about the winner: ArmyTwo.
        if (!battle.getArmyOne().hasUnits()) {
            BattleTitle.setText(battle.getArmyTwo().getName() + " won the battle with " + battle.getArmyTwo().getUnits().size() + " Unit(s) left!");
            NextKillButton.setDisable(true);
            FinishBattleButton.setDisable(true);
            //if ArmyTwo has no Units, the Page is updated with information about the winner: ArmyOne.
        } else if (!battle.getArmyTwo().hasUnits()) {
            BattleTitle.setText(battle.getArmyOne().getName() + " won the battle with " + battle.getArmyOne().getUnits().size() + " Unit(s) left!");
            NextKillButton.setDisable(true);
            FinishBattleButton.setDisable(true);
        }
    }
}
