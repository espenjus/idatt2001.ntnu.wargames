package ntnu.idatt2001.espenjus.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import ntnu.idatt2001.espenjus.wargamesPackage.Battle;
import ntnu.idatt2001.espenjus.wargamesPackage.Terrain;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller for ChooseBattleInfo page
 */
public class ChooseBattleInfoController implements Initializable {

    private Battle battle;
    private Terrain terrain = Terrain.HILL;
    private Image terrainImage;
    private Terrain[] terrains = {Terrain.HILL, Terrain.PLAINS, Terrain.FOREST};


    @FXML
    private Label TerrainName, TerrainInfo;

    @FXML
    private ImageView TerrainImage;

    @FXML
    private ChoiceBox<Terrain> TerrainChoice;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //sets ChoiceBox with different Terrains
        TerrainChoice.getItems().addAll(terrains);
        TerrainChoice.setValue(TerrainChoice.getItems().get(0));
        TerrainChoice.setOnAction(this::changeTerrainInfo);
    }

    /**
     * method for changing the Controllers terrain variable and updating the page with the new terrain's info
     * @param actionEvent
     */
    private void changeTerrainInfo(javafx.event.ActionEvent actionEvent) {
        this.terrain = TerrainChoice.getValue();
        setTerrainInfo(terrain);
    }

/**
 * method for giving the controller the variables needed
 */
    public void initData(Battle battle) {
        this.battle = battle;
        setTerrainInfo(terrain);
    }

    /**
     * Method that sets the different Labels and Image corresponding with the terrain variable passed in
     * @param terrain Enum Terrain
     */
    public void setTerrainInfo(Terrain terrain) {
        TerrainName.setText(terrain.name);
        TerrainInfo.setText(terrain.description);
        terrainImage = getTerrainImage(terrain);
        TerrainImage.setImage(getTerrainImage(terrain));
    }

    /**
     * method that returns a terrain's Image
     * @param terrain Enum Terrain
     * @return Image of Terrain
     */
    public Image getTerrainImage(Terrain terrain) {
        if (terrain.equals(Terrain.FOREST)) {
            try {
                return new Image(new FileInputStream("src/main/resources/ntnu/idatt2001/espenjus/View/Images/FOREST.jpg"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else if (terrain.equals(Terrain.PLAINS)) {
            try {
                return new Image(new FileInputStream("src/main/resources/ntnu/idatt2001/espenjus/View/Images/PLAINS.jpg"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else if (terrain.equals(Terrain.HILL)) {
            try {
                return new Image(new FileInputStream("src/main/resources/ntnu/idatt2001/espenjus/View/Images/HILL.jpg"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * method that changes scene to Battle page when corresponding button is pressed
     * @param event
     * @throws IOException
     */
    public void GoToBattle(ActionEvent event) throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../view/Battle.fxml"));
            Parent sceneParent = loader.load();
            Scene scene = new Scene(sceneParent);
            scene.setUserData(loader);
            BattleController controller = loader.getController();
            controller.initData(battle, terrain, terrainImage);

            Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.show();

        } catch (IOException e) {
            System.out.println(e.getMessage());
            throw e;
        }
    }
}
