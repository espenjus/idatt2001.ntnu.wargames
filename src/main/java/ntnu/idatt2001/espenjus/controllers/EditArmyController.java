package ntnu.idatt2001.espenjus.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import ntnu.idatt2001.espenjus.units.Unit;
import ntnu.idatt2001.espenjus.units.UnitFactory;
import ntnu.idatt2001.espenjus.units.TypeOfUnit;
import ntnu.idatt2001.espenjus.wargamesPackage.Army;
import ntnu.idatt2001.espenjus.wargamesPackage.Battle;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller for EditArmy page
 */
public class EditArmyController implements Initializable {

    private Battle battle;
    private Army army = new Army("Army");
    private Army copyArmy = new Army("Army");
    private boolean editedArmyIsArmyOne = true;
    private UnitFactory factory = new UnitFactory();
    private TypeOfUnit[] unitTypes = {TypeOfUnit.CAVALRYUNIT, TypeOfUnit.INFANTRYUNIT
            , TypeOfUnit.RANGEDUNIT, TypeOfUnit.COMMANDERUNIT};

    @FXML
    private TableView<Unit> TableOfArmy;

    @FXML
    private TableColumn<Unit, Integer> UnitHealth;

    @FXML
    private TableColumn<Unit, String> UnitName;

    @FXML
    private TableColumn<Unit, String> UnitType;

    @FXML
    private TextField ArmyName, InputUnitName, InputUnitHealth, InputNumberOfUnits;

    @FXML
    private Label AddFeedback, RemoveFeedback, AddError;

    @FXML
    private ChoiceBox<TypeOfUnit> UnitTypeBox;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //sets up the TableOfArmy with Units from Army to be edited
        UnitHealth.setCellValueFactory(new PropertyValueFactory<Unit, Integer>("health"));
        UnitName.setCellValueFactory(new PropertyValueFactory<Unit, String>("name"));
        UnitType.setCellValueFactory(new PropertyValueFactory<Unit, String>("type"));
        //sets up ChoiceBox with TypeOfUnit Enum list
        UnitTypeBox.getItems().addAll(unitTypes);
        UnitTypeBox.setValue(UnitTypeBox.getItems().get(0));
    }

    /**
     * method to give information to the controller from past page
     * @param battle Battle of the wargames
     * @param army Army to be edited
     */
    public void initData(Battle battle, Army army) {
        this.battle = battle;
        this.copyArmy = army;
        this.army.getUnits().addAll(copyArmy.getUnits());
        this.army.setName(copyArmy.getName());
        updateTableOfArmy();
        ArmyName.setPromptText(army.getName());

    }

    /**
     * method to set the EditArmyisArmyOne boolean
     * @param editedArmyIsArmyOne boolean
     */
    public void setEditedArmyIsArmyOne(boolean editedArmyIsArmyOne) {
        this.editedArmyIsArmyOne = editedArmyIsArmyOne;
    }

    /**
     * method to cancel the edit of Army and return to viewArmies page
     * @param actionEvent
     * @throws IOException if fxml load fails
     */
    public void CancelEditOfArmy(ActionEvent actionEvent) throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../view/ViewArmies.fxml"));
            Parent sceneParent = loader.load();
            Scene scene = new Scene(sceneParent);
            scene.setUserData(loader);
            ViewArmiesController controller = loader.getController();
            controller.initData(battle);

            Stage window = (Stage) ((Node)actionEvent.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.show();

        } catch (IOException e) {
            System.out.println(e.getMessage());
            throw e;
        }
    }

    /**
     * Gets all Units in army as an ObservableList
     * @return ObservableList of Units
     */
    public ObservableList<Unit> getUnits(){
        ObservableList<Unit> unitsObservable = FXCollections.observableArrayList();
        unitsObservable.addAll(army.getUnits());
        if(unitsObservable.size() == 0) return null;
        return unitsObservable;
    }

    /**
     * Updates Table of Army by setting all Units
     */
    @FXML
    public void updateTableOfArmy(){
        TableOfArmy.getItems().clear();
        ObservableList<Unit> unitsObservable = FXCollections.observableArrayList();
        if(getUnits() != null) {
            unitsObservable.addAll(getUnits());
            TableOfArmy.setItems(unitsObservable);
        }


    }

    /**
     * method for adding Units to the Army that is beeing edited if corresponding button is pressed
     * @param event
     */
    public void AddUnitsToArmy(ActionEvent event) {
        AddError.setText("");
        RemoveFeedback.setText("");
        AddFeedback.setText("");
        //gets information from the user
         TypeOfUnit unitType = UnitTypeBox.getValue();
        String unitName = InputUnitName.getText();
        //tries to use the user information to create and add units to the Army. Informs user if Successful
        //or if something went wrong
        try {
            int unitHealth = Integer.parseInt(InputUnitHealth.getText());
            int numberOfUnits = Integer.parseInt(InputNumberOfUnits.getText());
            if (checkIfUnitsCanBeAdded(unitName, unitHealth, numberOfUnits)) {
                List<Unit> unitsToBeAdded = factory.listOfUnits(numberOfUnits, unitType, unitName, unitHealth);
                army.getUnits().addAll(unitsToBeAdded);
                updateTableOfArmy();
                AddFeedback.setText("Successfully added " + numberOfUnits + " " + unitType + "(s)!");
            }
        } catch (NumberFormatException e) {
            AddError.setText("Unit health and number of Units has to be filled in as numbers");
        }
    }

    /**
     * method to remove a chosen Unit from edited Army if corresponding button is pressed
     * @param event
     */
    public void RemoveUnitFromArmy(ActionEvent event) {
        AddError.setText("");
        RemoveFeedback.setText("");
        AddFeedback.setText("");
        //Tries to remove the Unit the user has chosen and informs user if successful.
        try {
            int selectedID = TableOfArmy.getSelectionModel().getSelectedIndex();
            Unit unit = TableOfArmy.getItems().get(selectedID);
            army.getUnits().remove(selectedID);
            RemoveFeedback.setText(unit.getClass().getSimpleName() + " " + unit.getName() + " was removed!");
            updateTableOfArmy();
            //Informs user if no Unit is chosen
        } catch (IndexOutOfBoundsException e) {
            RemoveFeedback.setText("Please choose a Unit you want to remove!");
        }


    }

    /**
     * method for saving changes to the edited Army and return to the ViewArmies page if corresponding
     * button is pressed
     * @param event
     * @throws IOException if fxml load fails
     */
    public void SaveChangesToArmy(ActionEvent event) throws IOException {
        //sets name of army to user input if the user har inputted a new name
        if (!ArmyName.getText().isEmpty() && !ArmyName.getText().isBlank()) {
            army.setName(ArmyName.getText());
        }
        //sets the Battle's ArmyOne info if boolean editedArmyIsArmyOne is true
        if (editedArmyIsArmyOne) {
            battle.getArmyOne().getUnits().clear();
            battle.getArmyOne().getUnits().addAll(army.getUnits());
            battle.getArmyOne().setName(army.getName());
            //sets the Battle's ArmyTwo info if boolean editedArmyIsArmyOne is false
        } else {
            battle.getArmyTwo().getUnits().clear();
            battle.getArmyTwo().getUnits().addAll(army.getUnits());
            battle.getArmyTwo().setName(army.getName());
        }
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../view/ViewArmies.fxml"));
            Parent sceneParent = loader.load();
            Scene scene = new Scene(sceneParent);
            scene.setUserData(loader);
            ViewArmiesController controller = loader.getController();
            controller.initData(battle);

            Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.show();

        } catch (IOException e) {
            System.out.println(e.getMessage());
            throw e;
        }
    }

    /**
     * method for checking if the userinput for adding Units can be used to add Units to an Army.
     * Informs User if some input is wrong
     * @param unitName String name of Unit
     * @param health int Health of Unit
     * @param numberOfUnits int Number of units to be added
     * @return boolean
     */
    public boolean checkIfUnitsCanBeAdded(String unitName, int health, int numberOfUnits) {
        if (unitName.isEmpty() || unitName.isBlank()) {
            AddError.setText("You need to enter a UnitName!");
            return false;
        } else if (health <= 0) {
            AddError.setText("Units can't start a battle with less than 1 in health!");
            return false;
        } else if (numberOfUnits < 1) {
            AddError.setText("Cannot add zero or a negative number of Units!");
            return false;
        }

        return true;
    }
}
