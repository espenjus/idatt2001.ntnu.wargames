package ntnu.idatt2001.espenjus.units;

import java.util.ArrayList;
import java.util.List;

/**
 * Factory class that creates Units
 */

public class UnitFactory {


    /**
     * empty UnitFactory Constructor
     */
    public UnitFactory() {}

    /**
     * Method for creating a single Unit
     * @param type Enum TypeOfUnit you want to create
     * @param name String name of the Unit you want to create
     * @param health Int health of the Unit you want to create
     * @return a Unit
     */
    public Unit instanceOfUnit(TypeOfUnit type, String name, int health) {

        switch (type) {
            case CAVALRYUNIT -> {
                return new CavalryUnit(name, health);
            }
            case INFANTRYUNIT -> {
                return new InfantryUnit(name, health);
            }
            case RANGEDUNIT -> {
                return new RangedUnit(name, health);
            }
            case COMMANDERUNIT -> {
                return new CommanderUnit(name, health);
            }
            default -> {return null;}
        }

    }

    /**
     * Method for creating a list of units with same specifications
     * @param numberOfUnits int number of units you want to create
     * @param type TypeOfUnit you want to create a list of
     * @param name String name of the units in the list
     * @param health int health of the units in the list
     * @return List of Units
     */
    public List<Unit> listOfUnits(int numberOfUnits, TypeOfUnit type, String name, int health) {
        List<Unit> unitList = new ArrayList<>();
        for (int i = 0; i < numberOfUnits; i++) {
            switch (type) {
                case CAVALRYUNIT -> {
                    Unit unit = new CavalryUnit(name, health);
                    unitList.add(unit);
                }
                case INFANTRYUNIT -> {
                    Unit unit = new InfantryUnit(name, health);
                    unitList.add(unit);
                }
                case RANGEDUNIT -> {
                    Unit unit = new RangedUnit(name, health);
                    unitList.add(unit);
                }
                case COMMANDERUNIT -> {
                    Unit unit = new CommanderUnit(name, health);
                    unitList.add(unit);
                }
            }
        }
        return unitList;
    }
}
