package ntnu.idatt2001.espenjus.units;

import ntnu.idatt2001.espenjus.wargamesPackage.Terrain;

/**
 * Class for RangedUnit, a type of Unit
 */
public class RangedUnit extends Unit {

    protected final static int RANGED_ATTACK_BONUS = 3;
    protected final static int DEFAULT_RANGED_ATTACK = 15;
    protected final static int DEFAULT_RANGED_ARMOR = 8;
    protected int numberOfTimesAttacked = 0;
    private String type = getClass().getSimpleName();

    /**
     * constructor for the class, the variables described in the Unit-class
     * @param name String
     * @param health int
     * @param attack int
     * @param armor int
     */
    public RangedUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * constructor with attack and armor variables set to the default for all RangedUnits.
     * @param name String
     * @param health int
     */
    public RangedUnit(String name, int health) {
        super(name, health, DEFAULT_RANGED_ATTACK, DEFAULT_RANGED_ARMOR);
    }

    /**
     * Override method for taking damage which also counts the number of times the unit has been attacked.
     * this is used to calculate the resist bonus in the getResistBonus() method.
     * @param health int variable of the new health.
     */
    @Override
    public void takeDamage(int health) {
        super.takeDamage(health);
        numberOfTimesAttacked++;
    }

    /**
     * method for getting the attack bonus of RangedUnit. If it attacks from Terrain HILL, it the Hill-Bonus is added
     * If it attacks from Terrain FOREST, the Attack bonus is reduced by the FOREST-bonus.
     * @return int RANGED_ATTACK_BONUS
     */
    public int getAttackBonus(Terrain terrain) {
        if (terrain.equals(Terrain.HILL)) {
            return RANGED_ATTACK_BONUS + Terrain.HILL.bonus;
        } else if (terrain.equals(Terrain.FOREST)) {
            return RANGED_ATTACK_BONUS - Terrain.FOREST.bonus;
        }
        return RANGED_ATTACK_BONUS;
    }

    /**
     * method for getting the resist bonus of RangedUnit. because it has better protection from range, it should
     * return a bigger bonus the first time it is beeing attacked, a smaller bonus if it's the second attack
     * and a constant number if it has been attacked more than twice.
     * @return int resistBonus
     */
    public int getResistBonus(Terrain terrain) {
        if (numberOfTimesAttacked == 0) {return 6;}
        else if (numberOfTimesAttacked == 1) {return 4;}
        else {return 2;}
    }

    public String getType() {
        return type;
    }
}
