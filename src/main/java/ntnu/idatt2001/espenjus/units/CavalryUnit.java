package ntnu.idatt2001.espenjus.units;


import ntnu.idatt2001.espenjus.wargamesPackage.Terrain;

/**
 * class for a CavalryUnit, a type of class Unit
 */
public class CavalryUnit extends Unit {

    protected final static int CAVALRY_RESIST_BONUS = 1;
    protected final static int DEFAULT_CAVALRY_ATTACK = 20;
    protected final static int DEFAULT_CAVALRY_ARMOR = 12;
    protected final static int DEFAULT_CAVALRY_ATTACK_BONUS = 2;
    protected final static int FIRST_ATTACK_BONUS = 2;
    boolean hasAttacked = false;
    private String type = getClass().getSimpleName();

    /**
     * constructor for the class, variables described in the class Unit
     * @param name String
     * @param health int
     * @param attack int
     * @param armor int
     */
    public CavalryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * contructor for the class with attack and armor variables set to default for a CavalryUnit
     * @param name String
     * @param health int
     */
    public CavalryUnit(String name, int health) {
        super(name, health, DEFAULT_CAVALRY_ATTACK, DEFAULT_CAVALRY_ARMOR);
    }

    /**
     * overridden attack method, for the purpose of tracking if the CavalryUnit has attacked before.
     * this is needed in the getAttackBonus() method.
     * @param opponent Unit that is being attacked.
     */
    @Override
    public void attack(Unit opponent, Terrain terrain) {
        super.attack(opponent, terrain);
        this.hasAttacked = true;
    }

    /**
     * method for getting the attack bonus of the CavalryUnit. the method returns a +2 on the attack bonus
     * if it is the first attack of the CavalryUnit. The method also gives a bonus if the Terrain Enum is
     * of type Plains
     * @return int attack bonus
     */
    public int getAttackBonus(Terrain terrain) {
        if (!hasAttacked && terrain.equals(Terrain.PLAINS)) {
            return DEFAULT_CAVALRY_ATTACK_BONUS + FIRST_ATTACK_BONUS + Terrain.PLAINS.bonus;
        } else if (!hasAttacked) {
            return DEFAULT_CAVALRY_ATTACK_BONUS + FIRST_ATTACK_BONUS;
        } else if (terrain.equals(Terrain.PLAINS)) {
            return DEFAULT_CAVALRY_ATTACK_BONUS + Terrain.PLAINS.bonus;
        }
        else return DEFAULT_CAVALRY_ATTACK_BONUS;
    }

    /**
     * method for getting the resist bonus of a CavalryUnit.
     * If the Unit is in Forest Terrain, the method returns 0.
     * @return int resist bonus.
     */
    public int getResistBonus(Terrain terrain) {
        if (terrain.equals(Terrain.FOREST)) return 0;
         else return CAVALRY_RESIST_BONUS;
    }

    public String getType() {
        return type;
    }
}
