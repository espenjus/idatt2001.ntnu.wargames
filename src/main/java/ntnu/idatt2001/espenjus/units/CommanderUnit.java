package ntnu.idatt2001.espenjus.units;

/**
 * class for CommanderUnit, a type of CavalryUnit.
 */
public class CommanderUnit extends CavalryUnit {

    protected final static int DEFAULT_CAVALRY_ATTACK = 25;
    protected final static int DEFAULT_CAVALRY_ARMOR = 15;
    private String type = getClass().getSimpleName();

    /**
     * constructor for the class, variables described in the class Unit
     * @param name String
     * @param health int
     * @param attack int
     * @param armor int
     */
    public CommanderUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * constructor for the class with attack and armor set to default for a CommanderUnit.
     * @param name String
     * @param health int
     */
    public CommanderUnit(String name, int health) {
        super(name, health, DEFAULT_CAVALRY_ATTACK, DEFAULT_CAVALRY_ARMOR);
    }

    public String getType() {
        return type;
    }
}
