package ntnu.idatt2001.espenjus.units;

/**
 * Enum for different types of Units in the Wargames
 */
public enum TypeOfUnit {
    CAVALRYUNIT,
    INFANTRYUNIT,
    RANGEDUNIT,
    COMMANDERUNIT;
}
