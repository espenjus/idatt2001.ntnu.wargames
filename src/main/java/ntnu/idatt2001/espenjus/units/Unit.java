package ntnu.idatt2001.espenjus.units;

import ntnu.idatt2001.espenjus.wargamesPackage.Terrain;

/**
 * Abstract superclass representing a Unit
 */
public abstract class Unit {

    private String name;
    private int health;
    private int attack;
    private int armor;

    /**
     * constructor for class Unit
     * @param name String variable holding the name of a unit. cannot be blank.
     * @param health int variable holding the health of a unit. cannot be lower than 1
     * @param attack int variable holding the impact of the unit's attack. cannot be lower than 1.
     * @param armor int variable holding the resistance when attacked of the unit. cannot be lower than 1.
     */
    public Unit(String name, int health, int attack, int armor) {
        if (health <= 1 || attack <= 1 || armor <= 1) throw new IllegalArgumentException("Health, attack and armor all has to be 1 or over");
        if (name.isBlank()) throw new IllegalArgumentException("Name cannot be blank");
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }

    /**
     * method for when a unit attacks another unit. calculates an attackvalue based on attack and resist-bonus
     * variables of the units, subtracts that value from the opponent's health with the takeDamage method.
     * @param opponent Unit that is being attacked.
     */
    public void attack(Unit opponent, Terrain terrain) {
        int attackValue = 0;
        attackValue += (this.getAttack() + this.getAttackBonus(terrain));
        attackValue -= (opponent.getArmor() + opponent.getResistBonus(terrain));
        if (attackValue >= 0) {
            opponent.takeDamage(opponent.getHealth() - attackValue);
        }
    }

    /**
     * set-method for health, because the method should be overwritten in sub-classes,
     * it's called takeDamage
     * @param health int variable of the new health.
     */
    public void takeDamage(int health) {
        this.setHealth(health);
    }

    /**
     * get-method for name variable
     * @return String name
     */
    public String getName() {
        return name;
    }

    /**
     * get-method for health variable
     * @return int health
     */
    public int getHealth() {
        return health;
    }

    /**
     * get-method for attack variable
     * @return int attack
     */
    public int getAttack() {
        return attack;
    }

    /**
     * get-method for armor variable
     * @return int armor
     */
    public int getArmor() {
        return armor;
    }

    /**
     * set-method for health, can never be lower than zero, so if health variable is a negative number,
     * the method sets the value to zero.
     * @param health int, new health variable
     */
    public void setHealth(int health) {
        if (health < 0) {
            health = 0;
        }
        this.health = health;
    }

    /**
     * abstract method for getting an attack bonus
     * @return int
     */
    public abstract int getAttackBonus(Terrain terrain);

    /**
     * abstract method for getting resist bonus
     * @return int
     */
    public abstract int getResistBonus(Terrain terrain);

    /**
     * to-string method
     * @return String
     */
    public String toString() {
        return "Name: " + name + " Health: " + health + "Attack: " + attack + " Armor: " + armor;
    }
}

