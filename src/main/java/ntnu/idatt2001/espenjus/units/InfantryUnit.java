package ntnu.idatt2001.espenjus.units;

import ntnu.idatt2001.espenjus.wargamesPackage.Terrain;

/**
 * class for an InfantryUnit, a type of Unit.
 */
public class InfantryUnit extends Unit {
    protected final static int INFANTRY_ATTACK_BONUS = 2;
    protected final static int INFANTRY_RESIST_BONUS = 1;
    protected final static int DEFAULT_INFANTRY_ATTACK = 15;
    protected final static int DEFAULT_INFANTRY_ARMOR = 10;
    private String type = getClass().getSimpleName();

    /**
     * constructor for the class, variables described in class Unit
     * @param name String
     * @param health int
     * @param attack int
     * @param armor int
     */
    public InfantryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Constructor for the class with attack and armor variables set to default for all InfantryUnits.
     * @param name String
     * @param health int
     */
    public InfantryUnit(String name, int health) {
        super(name,health,DEFAULT_INFANTRY_ATTACK,DEFAULT_INFANTRY_ARMOR);
    }

    /**
     * method for getting the attack bonus for an InfantryUnit. returns a bigger value if the
     * Terrain the Unit is battling in is FOREST
     * @return int INFANTRY_ATTACK_BONUS.
     */
    public int getAttackBonus(Terrain terrain) {
        if (terrain.equals(Terrain.FOREST)) {
            return INFANTRY_ATTACK_BONUS + Terrain.FOREST.bonus;
        }
        return INFANTRY_ATTACK_BONUS;
    }

    /**
     * method for getting the resist bonus for an InfantryUnit. returns a bigger value if the
     * Terrain the Unit is battling in is FOREST
     * @return int INFANTRY_RESIST_BONUS
     */
    public int getResistBonus(Terrain terrain) {
        if (terrain.equals(Terrain.FOREST)) {
            return INFANTRY_RESIST_BONUS + Terrain.FOREST.bonus;
        }
        return INFANTRY_RESIST_BONUS;
    }

    public String getType() {
        return type;
    }
}

