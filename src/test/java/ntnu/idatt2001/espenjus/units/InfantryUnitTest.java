package ntnu.idatt2001.espenjus.units;

import ntnu.idatt2001.espenjus.wargamesPackage.Terrain;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class InfantryUnitTest {


    @Nested
    class InitiationOfInfantryUnitTest {
        @Test
        @DisplayName("Test for the InfantryUnit constructor receiving all variables")
        public void testConstructorWithAllVariables() {
            String name = "Testname";
            int health = 100;
            int attack = 20;
            int armor = 10;
            InfantryUnit unit = new InfantryUnit(name, health, attack, armor);
            assertSame(name, unit.getName());
            assertSame(health, unit.getHealth());
            assertSame(attack, unit.getAttack());
            assertSame(armor, unit.getArmor());
        }
        @Test
        @DisplayName("Test for the InfantryUnit constructor receiving name and health variables")
        public void testConstructorWithTwoVariables() {
            String name = "Testname";
            int health = 100;
            InfantryUnit unit = new InfantryUnit(name, health);
            assertSame(name, unit.getName());
            assertSame(health, unit.getHealth());
            assertSame(InfantryUnit.DEFAULT_INFANTRY_ATTACK, unit.getAttack());
            assertSame(InfantryUnit.DEFAULT_INFANTRY_ARMOR, unit.getArmor());
        }

        @Test
        @DisplayName("Tests the different exceptions that can happen while initiating a InfantryUnit")
        public void testExceptionsForInfantryUnits() {
            String name = "Name";
            String falseName = "";
            int health = 100;
            int falseHealh = -3;
            int falseAttack = -1;
            int falseResist = -1;
            assertThrows(IllegalArgumentException.class, () -> { InfantryUnit unit = new InfantryUnit(falseName, health);});
            assertThrows(IllegalArgumentException.class, () -> { InfantryUnit unit1 = new InfantryUnit(name,falseHealh);});
            assertThrows(IllegalArgumentException.class, () -> { InfantryUnit unit2 = new InfantryUnit(name, health, falseAttack, 3);});
            assertThrows(IllegalArgumentException.class, () -> { InfantryUnit unit3 = new InfantryUnit(name, health, 3, falseResist);});
        }
    }

    @Nested
    class ResistAndAttackBonusTest {

        @Test
        @DisplayName("Test the attack bonus method for InfantryUnit not in Terrain FOREST")
        public void testAttackBonus() {
            String name = "Testname";
            int health = 100;
            int attack = 20;
            int armor = 10;
            InfantryUnit unit = new InfantryUnit(name, health, attack, armor);
            assertSame(InfantryUnit.INFANTRY_ATTACK_BONUS, unit.getAttackBonus(Terrain.HILL));
        }

        @Test
        @DisplayName("Test the resist bonus method for InfantryUnit not in Terrain FOREST")
        public void testResistBonus() {
            String name = "Testname";
            int health = 100;
            int attack = 20;
            int armor = 10;
            InfantryUnit unit = new InfantryUnit(name, health, attack, armor);
            assertSame(InfantryUnit.INFANTRY_RESIST_BONUS, unit.getResistBonus(Terrain.HILL));
        }

        @Test
        @DisplayName("Test the attack bonus method for InfantryUnit in Terrain FOREST")
        public void testAttackBonusInTerrainForest() {
            String name = "Testname";
            int health = 100;
            int attack = 20;
            int armor = 10;
            InfantryUnit unit = new InfantryUnit(name, health, attack, armor);
            assertSame(InfantryUnit.INFANTRY_ATTACK_BONUS + Terrain.FOREST.bonus, unit.getAttackBonus(Terrain.FOREST));
        }

        @Test
        @DisplayName("Test the resist bonus method for InfantryUnit not in Terrain FOREST")
        public void testResistBonusInTerrainForest() {
            String name = "Testname";
            int health = 100;
            int attack = 20;
            int armor = 10;
            InfantryUnit unit = new InfantryUnit(name, health, attack, armor);
            assertSame(InfantryUnit.INFANTRY_RESIST_BONUS + Terrain.FOREST.bonus, unit.getResistBonus(Terrain.FOREST));
        }
    }
}
