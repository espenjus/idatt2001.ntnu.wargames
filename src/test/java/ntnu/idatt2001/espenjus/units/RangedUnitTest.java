package ntnu.idatt2001.espenjus.units;

import ntnu.idatt2001.espenjus.wargamesPackage.Terrain;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RangedUnitTest {


    @Nested
    class InitiationOfRangedUnit {

        @Test
        @DisplayName("Test the RangedUnit constructor receiving all variables")
        public void testConstructorWithAllVariables() {
            String name = "Testname";
            int health = 100;
            int attack = 20;
            int armor = 10;
            RangedUnit unit = new RangedUnit(name, health, attack, armor);
            assertSame(name, unit.getName());
            assertSame(health, unit.getHealth());
            assertSame(attack, unit.getAttack());
            assertSame(armor, unit.getArmor());
        }

        @Test
        @DisplayName("Test the RangedUnit constructor receiving name and health variables")
        public void testConstructorWithTwoVariables() {
            String name = "Testname";
            int health = 100;
            RangedUnit unit = new RangedUnit(name, health);
            assertSame(name, unit.getName());
            assertSame(health, unit.getHealth());
            assertSame(RangedUnit.DEFAULT_RANGED_ATTACK, unit.getAttack());
            assertSame(RangedUnit.DEFAULT_RANGED_ARMOR, unit.getArmor());
        }

        @Test
        @DisplayName("Tests the different exceptions that can happen while initiating a RangedUnit")
        public void testExceptionsForRangedUnits() {
            String name = "Name";
            String falseName = "";
            int health = 100;
            int falseHealh = -3;
            int falseAttack = -1;
            int falseResist = -1;
            assertThrows(IllegalArgumentException.class, () -> { RangedUnit unit = new RangedUnit(falseName, health);});
            assertThrows(IllegalArgumentException.class, () -> { RangedUnit unit1 = new RangedUnit(name,falseHealh);});
            assertThrows(IllegalArgumentException.class, () -> { RangedUnit unit2 = new RangedUnit(name, health, falseAttack, 3);});
            assertThrows(IllegalArgumentException.class, () -> { RangedUnit unit3 = new RangedUnit(name, health, 3, falseResist);});
        }
    }

    @Nested
    class resistAndAttackBonusTests {


        @Test
        @DisplayName("Test the RangedUnit attack bonus method not in Terrain HILL or FOREST")
        public void TestAttackBonusMethodNotInHillOrForest() {
            String name = "Testname";
            int health = 100;
            int attack = 20;
            int armor = 10;
            RangedUnit unit = new RangedUnit(name, health, attack, armor);
            assertSame(RangedUnit.RANGED_ATTACK_BONUS, unit.getAttackBonus(Terrain.PLAINS));
        }



        @Test
        @DisplayName("Test the RangedUnit resist bonus method and that it decreases after being attacked the first two times")
        public void testThatResistBonusMethodDecreasesAfterAttacked() {
            String name = "Testname";
            int health = 100;
            RangedUnit unit = new RangedUnit(name, health);
            RangedUnit unit1 = new RangedUnit(name, health);
            int resistBonus = unit.getResistBonus(Terrain.PLAINS);
            assertSame(6, resistBonus);

            unit1.attack(unit, Terrain.PLAINS);
            int resistBonusAfterAttacked = unit.getResistBonus(Terrain.PLAINS);
            assertTrue(resistBonusAfterAttacked < resistBonus);

            unit1.attack(unit, Terrain.PLAINS);
            int resistBonusAfterSecondAttack = unit.getResistBonus(Terrain.PLAINS);
            assertTrue(resistBonusAfterSecondAttack < resistBonusAfterAttacked);
        }

        @Test
        @DisplayName("Test that the RangedUnit attack bonus increases if the Terrain is HILL")
        public void TestThatAttackBonusMethodIncreasesInHill() {
            String name = "Testname";
            int health = 100;
            int attack = 20;
            int armor = 10;
            RangedUnit unit = new RangedUnit(name, health, attack, armor);
            assertTrue(unit.getAttackBonus(Terrain.HILL) > unit.getAttackBonus(Terrain.PLAINS));
        }

        @Test
        @DisplayName("Test that the RangedUnit attack bonus decreases if the Terrain is FOREST")
        public void TestThatAttackBonusMethodDecreasesInForest() {
            String name = "Testname";
            int health = 100;
            int attack = 20;
            int armor = 10;
            RangedUnit unit = new RangedUnit(name, health, attack, armor);
            assertTrue(unit.getAttackBonus(Terrain.FOREST) < unit.getAttackBonus(Terrain.PLAINS));
        }
    }
}