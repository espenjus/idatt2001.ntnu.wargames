package ntnu.idatt2001.espenjus.units;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class UnitFactoryTest {

    @Nested
    public class InstanceOfUnitMethodTests {
        @Test
        @DisplayName("Test the instanceOfUnit method with cavalryUnit")
        public void instanceOfUnitWithCavalryUnitTest() {
            UnitFactory factory = new UnitFactory();
            Unit unit = factory.instanceOfUnit(TypeOfUnit.CAVALRYUNIT, "testName", 100);
            assertEquals(CavalryUnit.class, unit.getClass());
        }

        @Test
        @DisplayName("Test the instanceOfUnit method with commanderUnit")
        public void instanceOfUnitWithCommanderUnitTest() {
            UnitFactory factory = new UnitFactory();
            Unit unit = factory.instanceOfUnit(TypeOfUnit.COMMANDERUNIT, "testName", 100);
            assertEquals(CommanderUnit.class, unit.getClass());
        }

        @Test
        @DisplayName("Test the instanceOfUnit method with InfantryUnit")
        public void instanceOfUnitWithInfantryUnitTest() {
            UnitFactory factory = new UnitFactory();
            Unit unit = factory.instanceOfUnit(TypeOfUnit.INFANTRYUNIT, "testName", 100);
            assertEquals(InfantryUnit.class, unit.getClass());
        }

        @Test
        @DisplayName("Test the instanceOfUnit method with RangedUnit")
        public void instanceOfUnitWithRangedUnitTest() {
            UnitFactory factory = new UnitFactory();
            Unit unit = factory.instanceOfUnit(TypeOfUnit.RANGEDUNIT, "testName", 100);
            assertEquals(RangedUnit.class, unit.getClass());
        }
    }

    @Nested
    public class ListOfUnitsMethodTests {
        @Test
        @DisplayName("Test that the listOfUnits method returns correct amount of units")
        public void correctNumberOfUnitsTest() {
            UnitFactory factory = new UnitFactory();
            int numberOfUnits = 5;
            List<Unit> testList = factory.listOfUnits(numberOfUnits, TypeOfUnit.CAVALRYUNIT, "name", 100);
            assertEquals(5, testList.size());
        }

        @Test
        @DisplayName("Test that the listOfUnits method returns correct type of CavalryUnits")
        public void listOfCavalryUnitsTest() {
            UnitFactory factory = new UnitFactory();
            int numberOfUnits = 2;
            List<Unit> testList = factory.listOfUnits(numberOfUnits, TypeOfUnit.CAVALRYUNIT, "name", 100);
            for (Unit unit : testList) {
                assertEquals(CavalryUnit.class, unit.getClass());
            }
        }

        @Test
        @DisplayName("Test that the listOfUnits method returns correct type of InfantryUnits")
        public void listOfInfantryUnitsTest() {
            UnitFactory factory = new UnitFactory();
            int numberOfUnits = 2;
            List<Unit> testList = factory.listOfUnits(numberOfUnits, TypeOfUnit.INFANTRYUNIT, "name", 100);
            for (Unit unit : testList) {
                assertEquals(InfantryUnit.class, unit.getClass());
            }
        }

        @Test
        @DisplayName("Test that the listOfUnits method returns correct type of RangedUnits")
        public void listOfInRangedUnitsTest() {
            UnitFactory factory = new UnitFactory();
            int numberOfUnits = 2;
            List<Unit> testList = factory.listOfUnits(numberOfUnits, TypeOfUnit.RANGEDUNIT, "name", 100);
            for (Unit unit : testList) {
                assertEquals(RangedUnit.class, unit.getClass());
            }
        }

        @Test
        @DisplayName("Test that the listOfUnits method returns correct type of CommanderUnits")
        public void listOfInCommanderUnitsTest() {
            UnitFactory factory = new UnitFactory();
            int numberOfUnits = 2;
            List<Unit> testList = factory.listOfUnits(numberOfUnits, TypeOfUnit.COMMANDERUNIT, "name", 100);
            for (Unit unit : testList) {
                assertEquals(CommanderUnit.class, unit.getClass());
            }
        }
    }





}
