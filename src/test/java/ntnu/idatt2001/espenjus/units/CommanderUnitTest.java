package ntnu.idatt2001.espenjus.units;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CommanderUnitTest {


    @Nested
    class InitiationOfRangedUnit {

        @Test
        @DisplayName("Test the CommanderUnit constructor receiving all variables")
        public void testConstructorWithAllVariables() {
            String name = "Testname";
            int health = 100;
            int attack = 20;
            int armor = 10;
            CommanderUnit unit = new CommanderUnit(name, health, attack, armor);
            assertSame(name, unit.getName());
            assertSame(health, unit.getHealth());
            assertSame(attack, unit.getAttack());
            assertSame(armor, unit.getArmor());
        }

        @Test
        @DisplayName("Test the CommanderUnit constructor receiving name and health variables")
        public void testConstructorWithTwoVariables() {
            String name = "Testname";
            int health = 100;
            CommanderUnit unit = new CommanderUnit(name, health);
            assertSame(name, unit.getName());
            assertSame(health, unit.getHealth());
            assertSame(CommanderUnit.DEFAULT_CAVALRY_ATTACK, unit.getAttack());
            assertSame(CommanderUnit.DEFAULT_CAVALRY_ARMOR, unit.getArmor());
        }

        @Test
        @DisplayName("Tests the different exceptions that can happen while initiating a CommanderUnit")
        public void testExceptionsForCommanderUnits() {
            String name = "Name";
            String falseName = "";
            int health = 100;
            int falseHealh = -3;
            int falseAttack = -1;
            int falseResist = -1;
            assertThrows(IllegalArgumentException.class, () -> { CommanderUnit unit = new CommanderUnit(falseName, health);});
            assertThrows(IllegalArgumentException.class, () -> { CommanderUnit unit1 = new CommanderUnit(name,falseHealh);});
            assertThrows(IllegalArgumentException.class, () -> { CommanderUnit unit2 = new CommanderUnit(name, health, falseAttack, 3);});
            assertThrows(IllegalArgumentException.class, () -> { CommanderUnit unit3 = new CommanderUnit(name, health, 3, falseResist);});
        }
    }

}
