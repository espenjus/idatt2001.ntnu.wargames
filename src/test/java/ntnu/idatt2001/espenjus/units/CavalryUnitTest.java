package ntnu.idatt2001.espenjus.units;

import ntnu.idatt2001.espenjus.wargamesPackage.Terrain;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CavalryUnitTest {

    @Nested
    class initiationOfCavalryUnit {

        @Test
        @DisplayName("Test the CavalryUnit constructor receiving all variables")
        public void testConstructorWithAllVariables() {
            String name = "Testname";
            int health = 100;
            int attack = 20;
            int armor = 10;
            CavalryUnit unit = new CavalryUnit(name, health, attack, armor);
            assertSame(name, unit.getName());
            assertSame(health, unit.getHealth());
            assertSame(attack, unit.getAttack());
            assertSame(armor, unit.getArmor());
        }

        @Test
        @DisplayName("Test the CavalryUnit constructor receiving name and health variables")
        public void testConstructorWithTwoVariables() {
            String name = "Testname";
            int health = 100;
            CavalryUnit unit = new CavalryUnit(name, health);
            assertSame(name, unit.getName());
            assertSame(health, unit.getHealth());
            assertSame(CavalryUnit.DEFAULT_CAVALRY_ATTACK, unit.getAttack());
            assertSame(CavalryUnit.DEFAULT_CAVALRY_ARMOR, unit.getArmor());
        }

    @Test
    @DisplayName("Tests the different exceptions that can happen while initiating a Unit")
    public void testExceptionsForCavalryUnits() {
        String name = "Name";
        String falseName = "";
        int health = 100;
        int falseHealh = -3;
        int falseAttack = -1;
        int falseResist = -1;
        assertThrows(IllegalArgumentException.class, () -> { CavalryUnit unit = new CavalryUnit(falseName, health);});
        assertThrows(IllegalArgumentException.class, () -> { CavalryUnit unit1 = new CavalryUnit(name,falseHealh);});
        assertThrows(IllegalArgumentException.class, () -> { CavalryUnit unit2 = new CavalryUnit(name, health, falseAttack, 3);});
        assertThrows(IllegalArgumentException.class, () -> { CavalryUnit unit3 = new CavalryUnit(name, health, 3, falseResist);});
    }
}


    @Nested
    @DisplayName("Resist and attack bonus tests")
    class resistAndAttackBonusTests {

        @Test
        @DisplayName("Test the CavalryUnit attack bonus method without being in PLAINS Terrain and that the bonus decreases after first attack")
        public void TestAttackBonusMethod() {
            String name = "Testname";
            int health = 100;
            CavalryUnit unit = new CavalryUnit(name, health);
            CavalryUnit unit1 = new CavalryUnit(name, health);
            int attackBonus = unit.getAttackBonus(Terrain.HILL);
            assertSame(CavalryUnit.DEFAULT_CAVALRY_ATTACK_BONUS + 2, attackBonus);
            unit.attack(unit1, Terrain.HILL);
            assertTrue(unit.getAttackBonus(Terrain.HILL) < attackBonus);
        }

        @Test
        @DisplayName("Test the CavalryUnit resist bonus method without being in FOREST Terrain")
        public void testResistBonusMethod() {
            String name = "Testname";
            int health = 100;
            CavalryUnit unit = new CavalryUnit(name, health);
            assertSame(CavalryUnit.CAVALRY_RESIST_BONUS, unit.getResistBonus(Terrain.HILL));
        }

        @Test
        @DisplayName("Test the CavalryUnit attack bonus method when in PLAINS Terrain and that the bonus decreases after first attack")
        public void TestAttackBonusInPlainsTerrainMethod() {
            String name = "Testname";
            int health = 100;
            CavalryUnit unit = new CavalryUnit(name, health);
            CavalryUnit unit1 = new CavalryUnit(name, health);
            int attackBonus = unit.getAttackBonus(Terrain.PLAINS);
            assertSame(CavalryUnit.DEFAULT_CAVALRY_ATTACK_BONUS + 2 + Terrain.FOREST.bonus, attackBonus);
            unit.attack(unit1, Terrain.HILL);
            assertTrue(unit.getAttackBonus(Terrain.HILL) < attackBonus);
        }

        @Test
        @DisplayName("Test the CavalryUnit resist bonus method without being in FOREST Terrain")
        public void testResistBonusInForestTerrainMethod() {
            String name = "Testname";
            int health = 100;
            CavalryUnit unit = new CavalryUnit(name, health);
            assertSame(0, unit.getResistBonus(Terrain.FOREST));
        }
    }
}
