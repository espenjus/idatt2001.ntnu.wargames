package ntnu.idatt2001.espenjus.wargamesPackage;

import ntnu.idatt2001.espenjus.units.*;
import org.junit.jupiter.api.*;


import java.util.LinkedHashMap;

import static org.junit.jupiter.api.Assertions.*;


public class BattleTest {
    private Unit testUnit1, testUnit2, testUnit3, testUnit4, testUnit5, testUnit6, testUnit7, testUnit8;
    private Army testArmy1, testArmy2;
    private Battle testBattle;

    @BeforeEach
    void setup() {
        testUnit1 = new CavalryUnit("Cavalry1", 100);
        testUnit2 = new CommanderUnit("Commander1", 100);
        testUnit3 = new InfantryUnit("Infantry1", 100);
        testUnit4 = new RangedUnit("Ranged1", 100);
        testArmy1 = new Army("Army1");
        testArmy1.addUnit(testUnit1); testArmy1.addUnit(testUnit2);
        testArmy1.addUnit(testUnit3); testArmy1.addUnit(testUnit4);

        testUnit5 = new CavalryUnit("Cavalry2", 100);
        testUnit6 = new CommanderUnit("Commander2", 100);
        testUnit7 = new InfantryUnit("Infantry2", 100);
        testUnit8 = new RangedUnit("Ranged2", 100);
        testArmy2 = new Army("Army2");
        testArmy2.addUnit(testUnit5); testArmy2.addUnit(testUnit6);
        testArmy2.addUnit(testUnit7); testArmy2.addUnit(testUnit8);

        testBattle = new Battle(testArmy1, testArmy2);
    }

    @AfterEach
    void undo() {
        testUnit1.setHealth(100);testUnit2.setHealth(100);testUnit3.setHealth(100);testUnit4.setHealth(100);
        testUnit5.setHealth(100);testUnit5.setHealth(100);testUnit7.setHealth(100);testUnit8.setHealth(100);

        testArmy1.addUnit(testUnit1); testArmy1.addUnit(testUnit2);
        testArmy1.addUnit(testUnit3); testArmy1.addUnit(testUnit4);

        testArmy2.addUnit(testUnit5); testArmy2.addUnit(testUnit6);
        testArmy2.addUnit(testUnit7); testArmy2.addUnit(testUnit8);

    }

    @Nested
    class MethodsUsedInBattleTest {

        @Test
        @DisplayName("Tests the takeDamage method for both positive and negative health input")
        public void testTakeDamageMethod() {
            int health = 5;
            CavalryUnit unit = new CavalryUnit("Name", 10);
            unit.takeDamage(health);
            assertEquals(unit.getHealth(), health);
            unit.takeDamage(-3);
            assertEquals(unit.getHealth(), 0);
        }

        @Test
        @DisplayName("Tests the Attack Method")
        public void testAttackMethod() {
            int health = 50;
            CavalryUnit unit = new CavalryUnit("Name", 5);
            CavalryUnit unit1 = new CavalryUnit("Name", health);
            unit.attack(unit1, Terrain.HILL);
            assertTrue(unit1.getHealth() < health);
        }
    }

    @Nested
    class simulationMethodTest {


        @Test
        @DisplayName("Tests that the simulate method returns an army with Units and that the loosing " +
                "army is empty. Terrain: HILL")
        public void testSimulationMethodInHill() {

            Army winningArmy = testBattle.simulate(Terrain.HILL);
            assertNotNull(winningArmy);
            assertTrue(winningArmy.hasUnits());
            assertTrue(testArmy1.getUnits().isEmpty() || testArmy2.getUnits().isEmpty());
        }

        @Test
        @DisplayName("Tests that the simulate method returns an army with Units and that the loosing army " +
                "is empty. Terrain: PLAINS")
        public void testSimulationMethodInPlains() {

            Army winningArmy = testBattle.simulate(Terrain.PLAINS);
            assertNotNull(winningArmy);
            assertTrue(winningArmy.hasUnits());
            assertTrue(testArmy1.getUnits().isEmpty() || testArmy2.getUnits().isEmpty());
        }

        @Test
        @DisplayName("Tests that the simulate method returns an army with Units and that the loosing army " +
                "is empty. Terrain: FOREST")
        public void testSimulationMethodInFOREST() {

            Army winningArmy = testBattle.simulate(Terrain.FOREST);
            assertNotNull(winningArmy);
            assertTrue(winningArmy.hasUnits());
            assertTrue(testArmy1.getUnits().isEmpty() || testArmy2.getUnits().isEmpty());
        }

        @Test
        @DisplayName("Test simulation to make sure the outcome is random")
        public void testSimulationResultsInRandomOutcome() {
            int numberOfTestRuns = 1000;
            int army1Wins = 0;
            int army2Wins = 0;
            for (int i = 0; i < numberOfTestRuns; i++) {
                setup();
                Army winningArmy = testBattle.simulate(Terrain.HILL);
                if (winningArmy.equals(testBattle.getArmyOne())) {
                    army1Wins++;
                } else if (winningArmy.equals(testBattle.getArmyTwo())) {
                    army2Wins++;
                }
            }
            System.out.println("army1 won " + army1Wins + " times");
            System.out.println("army2 won " + army2Wins + " times");
        }

    }

    @Nested
    public class methodForGettingInfoOnNextKillTest {

        @Test
        @DisplayName("Tests that the simulateNextKill method returns correct info on kill")
        public void simulateNextKillMethodReturnsCorrectInfo() {
            LinkedHashMap<Army, Unit> killInfo = testBattle.SimulateNextKill(Terrain.HILL);
            Army attackingArmy = killInfo.keySet().iterator().next();
            Army attackedArmy;
            if (attackingArmy.equals(testBattle.getArmyOne())) {
                attackedArmy = testBattle.getArmyTwo();
            } else {
                attackedArmy = testBattle.getArmyOne();
            }
            Unit attackingUnit = killInfo.get(attackingArmy);
            Unit killedUnit = killInfo.get(attackedArmy);
            // Tests that the attacking Unit is part of the attacking Army
            assertTrue(attackingArmy.getUnits().contains(attackingUnit));
            // Tests that the killed Unit is no longer part of the attacked Army
            assertFalse(attackedArmy.getUnits().contains(killedUnit));
        }
    }
}
