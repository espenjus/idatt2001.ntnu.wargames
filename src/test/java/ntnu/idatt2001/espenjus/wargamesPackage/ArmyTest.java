package ntnu.idatt2001.espenjus.wargamesPackage;

import ntnu.idatt2001.espenjus.units.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ArmyTest {
    private CavalryUnit cav1, cav2, cav3;
    private InfantryUnit inf1, inf2, inf3;
    private RangedUnit ran1, ran2, ran3;
    private CommanderUnit com1, com2, com3;
    private List<Unit> cavList, infList, ranList, comList;
    private Army cavArmy, infArmy, ranArmy, comArmy;


    @BeforeEach
    public void setup() {
        cav1 = new CavalryUnit("name", 100);
        cav2 = new CavalryUnit("name", 100);
        cav3 = new CavalryUnit("name", 100);

        inf1 = new InfantryUnit("name", 100);
        inf2 = new InfantryUnit("name", 100);
        inf3 = new InfantryUnit("name", 100);

        ran1 = new RangedUnit("name", 100);
        ran2 = new RangedUnit("name", 100);
        ran3 = new RangedUnit("name", 100);

        com1 = new CommanderUnit("Name", 100);
        com2 = new CommanderUnit("Name", 100);
        com3 = new CommanderUnit("Name", 100);

        cavList = new ArrayList<>(); cavList.add(cav1); cavList.add(cav2); cavList.add(cav3);
        infList = new ArrayList<>(); infList.add(inf1); infList.add(inf2); infList.add(inf3);
        ranList = new ArrayList<>(); ranList.add(ran1); ranList.add(ran1); ranList.add(ran1);
        comList = new ArrayList<>(); comList.add(com1);comList.add(com2); comList.add(com2);


        cavArmy = new Army("Army1", cavList);
        infArmy = new Army("Army2", infList);
        ranArmy = new Army("Army3", ranList);
        comArmy = new Army("Army4", comList);
    }

    @Nested
    class ArmyConstructorTests {

        @Test
        @DisplayName("Test the Army constructor receiving only name variable")
        public void testArmyConstructorWithNameVariable() {
            String name = "Testname";
            Army army = new Army(name);
            assertSame(name, army.getName());
        }

        @Test
        @DisplayName("Test the Army constructor receiving name and units list variable")
        public void testConstructorWithNameVariable() {
            String name = "Testname";
            int health = 100;
            CavalryUnit unit = new CavalryUnit(name, health);
            ArrayList<Unit> list = new ArrayList<>();
            list.add(unit);
            Army army = new Army(name, list);
            assertSame(list, army.getUnits());
        }

        @Test
        @DisplayName("Test initialization of a Army with blank name")
        public void testBlankName() {
            String name = "";
            assertThrows(IllegalArgumentException.class, () -> { Army army = new Army(name);});
        }
    }

    @Nested
    class ArmyMethodsTests {


        @Test
        @DisplayName("Test the method for adding a unit to the army")
        public void testAddMethod() {
            String name = "Testname";
            int health = 100;
            CavalryUnit unit = new CavalryUnit(name, health);
            Army army = new Army(name);
            army.addUnit(unit);
            assertTrue(army.getUnits().size() > 0);
            assertEquals(army.getUnits().get(0), unit);
        }

        @Test
        @DisplayName("Test the method for adding a list of units to the army")
        public void testAddAllMethod() {
            String name = "Testname";
            int health = 100;
            CavalryUnit unit = new CavalryUnit(name, health);
            CavalryUnit unit1 = new CavalryUnit(name, health);
            ArrayList<Unit> list = new ArrayList<>();
            list.add(unit); list.add(unit1);
            Army army = new Army(name);
            army.addAll(list);
            assertTrue(army.getUnits().size() > 0);
            assertEquals(army.getUnits().get(0), unit);
            assertEquals(army.getUnits().get(1), unit1);
        }

        @Test
        @DisplayName("Test the method for removing a unit from Army")
        public void testRemoveMethod() {
            String name = "Testname";
            int health = 100;
            CavalryUnit unit = new CavalryUnit(name, health);
            CavalryUnit unit1 = new CavalryUnit(name, health);
            ArrayList<Unit> list = new ArrayList<>();
            list.add(unit); list.add(unit1);
            Army army = new Army(name);
            army.addAll(list);
            army.remove(unit);
            assertFalse(army.getUnits().contains(unit));
        }

        @Test
        @DisplayName("Test the method for checking if an army contains units or not")
        public void testHasUnitsMethod() {
            String name = "Testname";
            int health = 100;
            CavalryUnit unit = new CavalryUnit(name, health);
            Army army = new Army(name);
            army.addUnit(unit);
            assertTrue(army.getUnits().contains(unit));
            army.remove(unit);
            assertFalse(army.getUnits().contains(unit));
        }

        @Test
        @DisplayName("Test the method for getting a random unit from an army")
        public void testGetRandomMethod() {
            String name = "Testname";
            int health = 100;
            CavalryUnit unit = new CavalryUnit(name, health);
            CavalryUnit unit1 = new CavalryUnit(name, health);
            Army army = new Army(name);
            army.addUnit(unit); army.addUnit(unit1);
            Unit testUnit = army.getRandom();
            assertTrue(army.getUnits().contains(testUnit));
        }

        @Test
        @DisplayName("Test for equals method and hashcode")
        public void testEqualsAndHashcodeMethods() {
            String name = "Testname";
            int health = 100;
            CavalryUnit unit = new CavalryUnit(name, health);
            Army army = new Army(name);
            Army army1 = new Army(name);
            assertEquals(army,army1);
            assertEquals(army.hashCode(),army1.hashCode());
            Army army2 = new Army("Testname2");
            assertNotEquals(army,army2);
            assertNotEquals(army.hashCode(),army2.hashCode());
        }

        @Test
        @DisplayName("Test the getInfantryUnits method with InfantryUnits in Army")
        public void getInfantryUnitsMethodTest() {
            assertEquals(3,infArmy.getInfantryUnits().size());
        }

        @Test
        @DisplayName("Test the getInfantryUnits method with CavalryUnits and no InfantryUnits in Army")
        public void getInfantryUnitsMethodWhenNoInfantryUnitsInArmyTest() {
            assertEquals(0, infArmy.getCavalryUnits().size());
        }

        @Test
        @DisplayName("Test the getCavalryUnits method with CavalryUnits in Army")
        public void getCavalryUnitsMethodTest() {
            assertEquals(3,cavArmy.getCavalryUnits().size());
        }

        @Test
        @DisplayName("Test the getCavalryUnits method with CommanderUnits in Army")
        public void getCavalryUnitsMethodWithCommanderUnitsInArmyTest() {
            assertEquals(0, cavArmy.getInfantryUnits().size());
        }

        @Test
        @DisplayName("Test the getRangedUnits method with RangedUnits in Army")
        public void getRangedUnitsMethodTest() {
            assertEquals(3, ranArmy.getRangedUnits().size());
        }

        @Test
        @DisplayName("Test the getRangedUnits method without RangedUnits in Army")
        public void getRangedUnitsMethodWithNoRangedUnitsInArmyTest() {
            assertEquals(0, infArmy.getRangedUnits().size());
        }

        @Test
        @DisplayName("Test the getCommanderUnits method")
        public void getCommanderUnitsMethodTest() {
            assertEquals(3, comArmy.getCommanderUnits().size());
        }

        @Test
        @DisplayName("Test the getCommanderUnits method without CommanderUnits in Army")
        public void getCommanderUnitsMethodWithNoCommanderUnitsInArmyTest() {
            assertEquals(0, cavArmy.getCommanderUnits().size());
        }
    }

}
