package ntnu.idatt2001.espenjus.wargamesPackage;

import ntnu.idatt2001.espenjus.units.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WargamesFileHandlerTest {
    private String DLM = File.separator;
    private String testName = "testArmy";
    private CavalryUnit cav1;
    private InfantryUnit inf1;
    private RangedUnit ran1;
    private CommanderUnit com1;

    private List<Unit> unitList;
    private Army testArmy;
    private WargamesFileHandler fileHandler;


    @BeforeEach
    public void setup() {
        cav1 = new CavalryUnit("name", 100);
        inf1 = new InfantryUnit("name", 100);
        ran1 = new RangedUnit("name", 100);
        com1 = new CommanderUnit("Name", 100);

        unitList = new ArrayList<>(); unitList.add(cav1); unitList.add(inf1); unitList.add(ran1); unitList.add(com1);

        testArmy = new Army(testName, unitList);

        fileHandler = new WargamesFileHandler();
        fileHandler.setFilePath("src" + DLM + "test" + DLM + "resources" + DLM);

    }

    @AfterEach
    public void removeFiles() {
        File file1 = new File(fileHandler.getFilePath() + testArmy.getName() + ".csv");
        if (file1.exists()) file1.delete();
    }





    @Test
    @DisplayName("Tests that the saveArmyToFile method creates a new file")
    public void saveArmyToFileMethodTest() throws IOException {
        fileHandler.saveArmyToFile(testArmy);
        File file = new File(fileHandler.getFilePath() + testArmy.getName() + ".csv");
        assertTrue(file.exists());
    }

    @Test
    @DisplayName("Tests that correct ArmyName is read from the readArmyFromFile method")
    public void correctArmyNameFromReadArmyFromFileMethodTest() throws IOException {
        fileHandler.saveArmyToFile(testArmy);
        Army army = fileHandler.readArmyFromFile(testName);
        assertEquals(testName, army.getName());
    }

    @Test
    @DisplayName("Tests that all units from file is added to army in readArmyFromFile method")
    public void readArmyFromFileMethodTest() throws IOException {
        fileHandler.saveArmyToFile(testArmy);
        Army army = fileHandler.readArmyFromFile(testName);
        assertEquals(4, army.getUnits().size());
    }

    @Test
    @DisplayName("Tests that readArmyFromFile method throws exception if there's no file with input armyName")
    public void readArmyFromFileMethodThrowsExceptionWhenNoFileFoundTest() {
        assertThrows(IllegalArgumentException.class, () -> { Army army = fileHandler.readArmyFromFile("WrongName");});
    }

    @Test
    @DisplayName("Tests that readArmyFromFile method throws exception if ArmyName line is empty")
    public void readArmyFromFileMethodThrowsExceptionWhenFirstLineOfFileIsEmpty() throws IOException {
        assertThrows(IllegalArgumentException.class, () -> { Army army = fileHandler.readArmyFromFile("NoInfoOnFirstLine");});
    }

    @Test
    @DisplayName("Tests if the readArmyFromFile method throws exception if a line misses a variable")
    public void readArmyFromFileThrowsExceptionIfLinesMissesVariables() throws IOException {
        assertThrows(IllegalArgumentException.class, () -> {Army army = fileHandler.readArmyFromFile("NotEnoughVariables");});
    }

    @Test
    @DisplayName("Tests if the readArmyFromFile method throws exception if name of Unit is missing in a line")
    public void readArmyFromFileThrowsExceptionIfNameOfUnitMissesFromLine() {
        assertThrows(IllegalArgumentException.class, () -> {Army army = fileHandler.readArmyFromFile("MissingUnitName");});
    }

    @Test
    @DisplayName("Tests if the readArmyFromFile method throws exception if health variable is not an int")
    public void readArmyFromFilThrowsExceptionIfHealthValueIsNotAnInt() {
        assertThrows(IOException.class, () -> {Army army = fileHandler.readArmyFromFile("WrongDataInHealthVariable");});
    }

    @Test
    @DisplayName("Tests if the readArmyFromFile method throws exception if Unittype is not found")
    public void readArmyFromFileThrowsExceptionIfUnitTypeIsNotFound() {
        assertThrows(IllegalArgumentException.class, () -> {Army army = fileHandler.readArmyFromFile("NotATypeOfUnit");});

    }

    @Test
    @DisplayName("Tests if the readArmyFromFile method throws exception if delimiter in file is not a comma")
    public void readArmyFromFileThrowsExceptionIfDelimiterIsNotComma() {
        assertThrows(IllegalArgumentException.class, () -> {Army army = fileHandler.readArmyFromFile("NotATypeOfUnit");});
    }


}
